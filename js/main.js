var DSSV = [];
// get data from localSatorage
getDataFromLocalStorage();

// add more student
function addStudent() {
  // check duplicate
  var inforStudent = getInforFromForm();
  var isValid =
    // if validate for one element => use &&

    isDuplicate(DSSV, inforStudent, 'maSV', 'spanMaSV', 'Mã Sinh Viên') &&
    isLength(inforStudent.maSV, 'spanMaSV', 4, 6);

  // if validate combine with elements => &

  // check email
  isValid &=
    isEmail(inforStudent.email) &&
    isDuplicate(DSSV, inforStudent, 'email', 'spanEmailSV', 'Email');

  // // check password
  isValid &=
    isPassword(inforStudent.matKhau) &&
    isLength(inforStudent.matKhau, 'spanMatKhau', 8, 12);

  // check Toan Score
  isValid &= isScore(inforStudent.diemToan, 'spanToan');

  // check Ly Score
  isValid &= isScore(inforStudent.diemLy, 'spanLy');

  // check Hoa Score
  isValid &= isScore(inforStudent.diemHoa, 'spanHoa');

  // check Name
  isValid &= checkName(inforStudent.tenSV, 'spanTenSV');

  if (isValid) {
    // push element at to array
    DSSV.push(inforStudent);
    // set data to localStorage
    setDataLocalStorage();
    // render DSSV to table
    renderDSSV(DSSV);
    // reset form
    resetForm();
  }
}

// delete student
function deleteStudent(id) {
  var foundIndex = DSSV.findIndex(function (item) {
    return item.maSV === id;
  });

  if (foundIndex !== -1) {
    // -1 is not found => !== -1 found
    DSSV.splice(foundIndex, 1);
  }

  setDataLocalStorage();
  renderDSSV(DSSV);
}

// editor student
function editStudent(id) {
  var foundIndex = DSSV.findIndex(function (item) {
    return item.maSV === id;
  });

  inforStudent = DSSV[foundIndex];
  fillInforStudent(inforStudent);
}

// update student
function updateStudent() {
  getInforFromForm();
  var foundIndex = DSSV.findIndex(function (item) {
    return item.maSV === getInforFromForm().maSV;
  });

  DSSV[foundIndex] = getInforFromForm();
  setDataLocalStorage();
  renderDSSV(DSSV);
}

// reset form
function resetStudent() {
  resetForm();
}
function searchStudent() {
  // get name student
  var getNameStudent = domID('txtSearch').value;

  // search student
  var filteredStudents = DSSV.filter(function (item) {
    console.log('filteredStudents before: ', filteredStudents);
    return removeDiacritic(item.tenSV).includes(
      removeDiacritic(getNameStudent)
    );
  });

  // Render students to screen
  if (filteredStudents.length > 0) {
    DSSV = filteredStudents;
    setDataLocalStorage();
    renderDSSV(DSSV);
  }
}
