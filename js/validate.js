// import { add } from './method';

// function show message
function showMessage(idErr, message) {
  domID(idErr).innerHTML = message;
}

// function check duplicate
// isValid => true

// item is the student, e.g. {id: 1, name:"John", age: 20}
// key is the property name, e.g. id | name | age
// return true if key is duplicated
function isDuplicate(DSSV, inforStudent, key, idErr, tagErr) {
  var foundIndex = DSSV.findIndex((item) => item[key] === inforStudent[key]);

  if (foundIndex > -1) {
    showMessage(idErr, tagErr + ` đã tồn tại`);
    return false; // duplicated
  }
  showMessage(idErr, ``);
  return true; // okey
}
// usage:
// var isDuplicateCode = isDuplicate(list, student, 'mssv');
// var isDuplicateEmail = isDuplicate(list, student, 'email');

// function check email validate
function isEmail(email) {
  const regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regEmail.test(email)) {
    showMessage('spanEmailSV', '');
    return true;
  } else if (email.length === 0) {
    showMessage('spanEmailSV', 'Email không được để trống');
    return false;
  } else {
    showMessage('spanEmailSV', 'Email không hợp lệ');
    return false;
  }
}

// function check length
function isLength(value, idErr, min, max) {
  var length = value.length;
  if (length <= max && length >= min) {
    showMessage(idErr, '');
    return true;
  } else {
    showMessage(idErr, `Trường này có độ dài từ ${min} đến ${max}`);
    return false;
  }
}

// function check password
function isPassword(password) {
  const regPassWord =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  if (regPassWord.test(password)) {
    showMessage('spanMatKhau', '');
    return true;
  } else if (password.length === 0) {
    showMessage('spanMatKhau', 'Password không được để trống');
    return false;
  } else {
    showMessage('spanMatKhau', 'Password không hợp lệ');
    return false;
  }
}

// function check score
function isScore(score, idErr) {
  if (score >= 0 && score <= 10 && score.toString().length !== 0) {
    showMessage(idErr, '');
    return true;
  } else if (score.toString().length === 0) {
    showMessage(idErr, 'Trường này không được để trống');
    return false;
  }
  showMessage(idErr, 'Điểm phải từ 0 đến 10');
  return false;
}

// function check name
function checkName(name, idErr) {
  if (name.length > 0) {
    showMessage(idErr, '');
    return true;
  } else {
    showMessage(idErr, 'Trường này không được để trống');
    return false;
  }
}
