// 1. function dom element
function domID(id) {
  return document.getElementById(id);
}
// 2. function get information from form
function getInforFromForm() {
  var maSV = domID('txtMaSV').value;
  var tenSV = domID('txtTenSV').value;
  var email = domID('txtEmail').value;
  var matKhau = domID('txtPass').value;
  var diemToan = domID('txtDiemToan').value;
  var diemLy = domID('txtDiemLy').value;
  var diemHoa = domID('txtDiemHoa').value;
  return new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);
  // {
  //   maSV: maSV,
  //   tenSV: tenSV,
  //   email: email,
  //   matKhau: matKhau,
  //   diemToan: diemToan,
  //   diemLy: diemLy,
  //   diemHoa: diemHoa,
  // };
}
// 3. function render DSSV
function renderDSSV(DSSV) {
  var contentHTML = '';
  var len = DSSV.length;
  for (var i = len - 1; i >= 0; i--) {
    var item = DSSV[i];
    var contentTR = `
    <tr>
    <td>${item.maSV}</td>
    <td>${item.tenSV}</td>
    <td>${item.email}</td>
    <td>${item.tinhDTB()}</td>
    <td>
    <button class='btn btn-danger' onclick='deleteStudent("${
      item.maSV
    }")'>Xoá</button>
    </td>
    <td>
    <button class='btn btn-warning' onclick='editStudent("${
      item.maSV
    }")'>Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += contentTR;
  }
  domID('tbodySinhVien').innerHTML = contentHTML;
}

// 4. function set data to localStorage
const DSSV_Local = 'DSSV_Local';
function setDataLocalStorage() {
  var dataJSON = JSON.stringify(DSSV);
  localStorage.setItem(DSSV_Local, dataJSON);
}
// 5. function get data from localStorage
function getDataFromLocalStorage() {
  var JSONdata = localStorage.getItem(DSSV_Local);
  if (JSONdata !== null) {
    // JSON.parse(jsonData) => array
    // convert array cũ ( lấy localStorage ) => không có key tinhDTB() => khi lưu xuống bị mất => khi lấy lên ko còn => convert thành array mới
    DSSV = JSON.parse(JSONdata).map(function (item) {
      // item : phần tử của array trong các lần lặp
      // return của map()
      return new SinhVien(
        item.maSV,
        item.tenSV,
        item.email,
        item.matKhau,
        item.diemToan,
        item.diemLy,
        item.diemHoa
      );
    });
    // map js
  }
  console.log('JSOaaaNdata: ', JSONdata);

  renderDSSV(DSSV);
}
// 6. function reset form
function resetForm() {
  domID('formQLSV').reset();
}

// 7. function fill in the form
function fillInforStudent(inforStudent) {
  domID('txtMaSV').value = inforStudent.maSV;
  domID('txtTenSV').value = inforStudent.tenSV;
  domID('txtEmail').value = inforStudent.email;
  domID('txtPass').value = inforStudent.pass;
  domID('txtDiemToan').value = inforStudent.diemToan;
  domID('txtDiemLy').value = inforStudent.diemLy;
  domID('txtDiemHoa').value = inforStudent.diemHoa;
}

// 8. function remove diacritic

/**
 * Method:
var getNameStudent = 'Nguyễn Thanh Tùng';
var normalizedNameStudent = getNameStudent
  .normalize('NFD')
  .replace(/[\u0300-\u036f]/g, '');
console.log('normalizedNameStudent: ', normalizedNameStudent);
 */
function removeDiacritic(string) {
  var r = string.toLowerCase();
  r = r.replace(new RegExp(/\s/g), '');
  r = r.replace(new RegExp(/[àáâãäåầ]/g), 'a');
  r = r.replace(new RegExp(/æ/g), 'ae');
  r = r.replace(new RegExp(/ç/g), 'c');
  r = r.replace(new RegExp(/[èéêë]/g), 'e');
  r = r.replace(new RegExp(/[ìíîï]/g), 'i');
  r = r.replace(new RegExp(/ñ/g), 'n');
  r = r.replace(new RegExp(/[òóôõöọ]/g), 'o');
  r = r.replace(new RegExp(/œ/g), 'oe');
  r = r.replace(new RegExp(/[ùúûü]/g), 'u');
  r = r.replace(new RegExp(/[ýÿ]/g), 'y');
  r = r.replace(new RegExp(/\W/g), '');
  return r;
}

// 9. function check include string
function isSubstring(parentString, substring) {
  if (parentString.includes(substring)) {
    return true;
  }
  return false;
}
console.log(isSubstring('nguyen thanh tung', `thanh`));
